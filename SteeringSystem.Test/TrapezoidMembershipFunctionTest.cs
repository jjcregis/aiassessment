﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SteeringSystem;
using NUnit.Framework;

namespace SteeringSystem.Test
{
    public class TrapezoidMembershipFunctionTest
    {
        private TrapezoidMembershipFunction _neg;
        private TrapezoidMembershipFunction _negAndPos;
        private TrapezoidMembershipFunction _posAndShort;

        [TestFixtureSetUp]
        public void SetUp()
        {
            _neg = new TrapezoidMembershipFunction(-3f, -2f, -1f, 0f, 1f);
            _negAndPos = new TrapezoidMembershipFunction(-2f, 0f, 1f, 2f, 1f);
            _posAndShort = new TrapezoidMembershipFunction(1f, 2f, 3f, 4f, .5f);
        }

        [Test]
        public void MembershipOf_XTooFarLeft_ZeroReturned()
        {
            Assert.IsTrue(_neg.MembershipOf(-3.1f).NearlyEquals(0f));
            Assert.IsTrue(_negAndPos.MembershipOf(-2.001f).NearlyEquals(0f));
            Assert.IsTrue(_posAndShort.MembershipOf(.99f).NearlyEquals(0f));
        }

        [Test]
        public void MembershipOf_XTooFarRight_ZeroReturned()
        {
            Assert.IsTrue(_neg.MembershipOf(0.1f).NearlyEquals(0f));
            Assert.IsTrue(_negAndPos.MembershipOf(2.001f).NearlyEquals(0f));
            Assert.IsTrue(_posAndShort.MembershipOf(4.01f).NearlyEquals(0f));
        }

        [Test]
        public void MembershipOf_XOnBottomLeftPoint_ValueReturned()
        {
            Assert.IsTrue(_neg.MembershipOf(-3f).NearlyEquals(0f));
            Assert.IsTrue(_negAndPos.MembershipOf(-2f).NearlyEquals(0f));
            Assert.IsTrue(_posAndShort.MembershipOf(1f).NearlyEquals(0f));
        }

        [Test]
        public void MembershipOf_XOnBottomRightPoint_ValueReturned()
        {
            Assert.IsTrue(_neg.MembershipOf(0f).NearlyEquals(0f));
            Assert.IsTrue(_negAndPos.MembershipOf(2f).NearlyEquals(0f));
            Assert.IsTrue(_posAndShort.MembershipOf(4f).NearlyEquals(0f));
        }

        [Test]
        public void MembershipOf_XOnTopLeftPoint_ValueReturned()
        {
            Assert.IsTrue(_neg.MembershipOf(-2f).NearlyEquals(1f));
            Assert.IsTrue(_negAndPos.MembershipOf(0f).NearlyEquals(1f));
            Assert.IsTrue(_posAndShort.MembershipOf(2f).NearlyEquals(.5f));
        }

        [Test]
        public void MembershipOf_XOnTopRightPoint_ValueReturned()
        {
            Assert.IsTrue(_neg.MembershipOf(-1f).NearlyEquals(1f));
            Assert.IsTrue(_negAndPos.MembershipOf(1f).NearlyEquals(1f));
            Assert.IsTrue(_posAndShort.MembershipOf(3f).NearlyEquals(.5f));
        }

        [Test]
        public void MembershipOf_XOnRisingSide_ValueReturned()
        {
            Assert.IsTrue(_neg.MembershipOf(-2.5f).NearlyEquals(.5f));
            Assert.IsTrue(_negAndPos.MembershipOf(-1f).NearlyEquals(.5f));
            Assert.IsTrue(_posAndShort.MembershipOf(1.5f).NearlyEquals(.25f));
        }

        [Test]
        public void MembershipOf_XOnTopSide_ValueReturned()
        {
            Assert.IsTrue(_neg.MembershipOf(-1.5f).NearlyEquals(1f));
            Assert.IsTrue(_negAndPos.MembershipOf(.5f).NearlyEquals(1f));
            Assert.IsTrue(_posAndShort.MembershipOf(2.5f).NearlyEquals(.5f));
        }
        
        [Test]
        public void MembershipOf_XOnFallingSide_ValueReturned()
        {
            Assert.IsTrue(_neg.MembershipOf(-.5f).NearlyEquals(.5f));
            Assert.IsTrue(_negAndPos.MembershipOf(1.5f).NearlyEquals(.5f));
            Assert.IsTrue(_posAndShort.MembershipOf(3.5f).NearlyEquals(.25f));
        }

        [Test]
        public void ValueWithMembership_YOnRisingSide_XReturned()
        {
            Assert.IsTrue(_neg.ValueWithMembership(0f, -3f, -2f).NearlyEquals(-3f));
            Assert.IsTrue(_neg.ValueWithMembership(.5f, -3f, -2f).NearlyEquals(-2.5f));
            Assert.IsTrue(_neg.ValueWithMembership(1f, -3f, -2f).NearlyEquals(-2f));

            Assert.IsTrue(_negAndPos.ValueWithMembership(0f, -2f, 0f).NearlyEquals(-2f));
            Assert.IsTrue(_negAndPos.ValueWithMembership(.5f, -2f, 0f).NearlyEquals(-1f));
            Assert.IsTrue(_negAndPos.ValueWithMembership(1f, -2f, 0f).NearlyEquals(0f));

            Assert.IsTrue(_posAndShort.ValueWithMembership(0f, 1f, 2f).NearlyEquals(1f));
            Assert.IsTrue(_posAndShort.ValueWithMembership(.25f, 1f, 2f).NearlyEquals(1.5f));
            Assert.IsTrue(_posAndShort.ValueWithMembership(.5f, 1f, 2f).NearlyEquals(2f));
        }

        [Test]
        public void ValueWithMembership_YOnTopSide_XReturned()
        {
            Assert.IsTrue(_neg.ValueWithMembership(1f, -2f, -1f).NearlyEquals(-2f));
            Assert.IsTrue(_negAndPos.ValueWithMembership(1f, 0f, 1f).NearlyEquals(0f));
            Assert.IsTrue(_posAndShort.ValueWithMembership(.5f, 2f, 3f).NearlyEquals(2f));
        }

        [Test]
        public void ValueWithMembership_YOnFallingSide_XReturned()
        {
            Assert.IsTrue(_neg.ValueWithMembership(1f, -1f, 0f).NearlyEquals(-1f));
            Assert.IsTrue(_neg.ValueWithMembership(.5f, -1f, 0f).NearlyEquals(-.5f));
            Assert.IsTrue(_neg.ValueWithMembership(0f, -1f, 0f).NearlyEquals(0f));

            Assert.IsTrue(_negAndPos.ValueWithMembership(1f, 1f, 2f).NearlyEquals(1f));
            Assert.IsTrue(_negAndPos.ValueWithMembership(.5f, 1f, 2f).NearlyEquals(1.5f));
            Assert.IsTrue(_negAndPos.ValueWithMembership(0f, 1f, 2f).NearlyEquals(2f));

            Assert.IsTrue(_posAndShort.ValueWithMembership(.5f, 3f, 4f).NearlyEquals(3f));
            Assert.IsTrue(_posAndShort.ValueWithMembership(.25f, 3f, 4f).NearlyEquals(3.5f));
            Assert.IsTrue(_posAndShort.ValueWithMembership(0f, 3f, 4f).NearlyEquals(4f));
        }
    }
}
