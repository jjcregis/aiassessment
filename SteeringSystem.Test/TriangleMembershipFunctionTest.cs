﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SteeringSystem;
using NUnit.Framework;

namespace SteeringSystem.Test
{
    public class TriangleMembershipFunctionTest
    {
        private TriangleMembershipFunction _neg;
        private TriangleMembershipFunction _negAndPos;
        private TriangleMembershipFunction _posAndShort;

        [TestFixtureSetUp]
        public void SetUp()
        {
            _neg = new TriangleMembershipFunction(-2f, -1f, 0f, 1f);
            _negAndPos = new TriangleMembershipFunction(-1f, 0f, 2f, 1f);
            _posAndShort = new TriangleMembershipFunction(1f, 2f, 3f, .5f);
        }

        [Test]
        public void MembershipOf_XTooFarLeft_ZeroReturned()
        {
            Assert.IsTrue(_neg.MembershipOf(-2.1f).NearlyEquals(0f));
            Assert.IsTrue(_negAndPos.MembershipOf(-1.001f).NearlyEquals(0f));
            Assert.IsTrue(_posAndShort.MembershipOf(.99f).NearlyEquals(0f));
        }

        [Test]
        public void MembershipOf_XTooFarRight_ZeroReturned()
        {
            Assert.IsTrue(_neg.MembershipOf(.1f).NearlyEquals(0f));
            Assert.IsTrue(_negAndPos.MembershipOf(2.001f).NearlyEquals(0f));
            Assert.IsTrue(_posAndShort.MembershipOf(3.1f).NearlyEquals(0f));
        }

        [Test]
        public void MembershipOf_XOnLeftPoint_ValueReturned()
        {
            Assert.IsTrue(_neg.MembershipOf(-2f).NearlyEquals(0f));
            Assert.IsTrue(_negAndPos.MembershipOf(-1f).NearlyEquals(0f));
            Assert.IsTrue(_posAndShort.MembershipOf(1f).NearlyEquals(0f));
        }

        [Test]
        public void MembershipOf_XOnRightPoint_ValueReturned()
        {
            Assert.IsTrue(_neg.MembershipOf(0f).NearlyEquals(0f));
            Assert.IsTrue(_negAndPos.MembershipOf(2f).NearlyEquals(0f));
            Assert.IsTrue(_posAndShort.MembershipOf(3f).NearlyEquals(0f));
        }

        [Test]
        public void MembershipOf_XOnMiddlePoint_ValueReturned()
        {
            Assert.IsTrue(_neg.MembershipOf(-1f).NearlyEquals(1f));
            Assert.IsTrue(_negAndPos.MembershipOf(0f).NearlyEquals(1f));
            Assert.IsTrue(_posAndShort.MembershipOf(2f).NearlyEquals(.5f));
        }

        [Test]
        public void MembershipOf_XOnRisingSide_ValueReturned()
        {
            Assert.IsTrue(_neg.MembershipOf(-1.5f).NearlyEquals(.5f));
            Assert.IsTrue(_negAndPos.MembershipOf(-.5f).NearlyEquals(.5f));
            Assert.IsTrue(_posAndShort.MembershipOf(1.5f).NearlyEquals(.25f));
        }

        [Test]
        public void MembershipOf_XOnFallingSide_ValueReturned()
        {            
            Assert.IsTrue(_neg.MembershipOf(-.5f).NearlyEquals(.5f));
            Assert.IsTrue(_negAndPos.MembershipOf(1f).NearlyEquals(.5f));
            Assert.IsTrue(_posAndShort.MembershipOf(2.5f).NearlyEquals(.25f));             
        }

        [Test]
        public void ValueWithMembership_YOnRisingSide_XReturned()
        {

            Assert.IsTrue(_neg.ValueWithMembership(0f, -2f, -1f).NearlyEquals(-2f));
            Assert.IsTrue(_neg.ValueWithMembership(.5f, -2f, -1f).NearlyEquals(-1.5f));
            Assert.IsTrue(_neg.ValueWithMembership(1f, -2f, -1f).NearlyEquals(-1f));

            Assert.IsTrue(_negAndPos.ValueWithMembership(0f, -1f, 0f).NearlyEquals(-1f));
            Assert.IsTrue(_negAndPos.ValueWithMembership(.5f, -1f, 0f).NearlyEquals(-.5f));
            Assert.IsTrue(_negAndPos.ValueWithMembership(1f, -1f, 0f).NearlyEquals(0f));

            Assert.IsTrue(_posAndShort.ValueWithMembership(0f, 1f, 2f).NearlyEquals(1f));
            Assert.IsTrue(_posAndShort.ValueWithMembership(.25f, 1f, 2f).NearlyEquals(1.5f));
            Assert.IsTrue(_posAndShort.ValueWithMembership(.5f, 1f, 2f).NearlyEquals(2f));
        }

        [Test]
        public void ValueWithMembership_YOnFallingSide_XReturned()
        {
            Assert.IsTrue(_neg.ValueWithMembership(1f, -1f, 0f).NearlyEquals(-1f));
            Assert.IsTrue(_neg.ValueWithMembership(.5f, -1f, 0f).NearlyEquals(-.5f));
            Assert.IsTrue(_neg.ValueWithMembership(0f, -1f, 0f).NearlyEquals(0f));
            
            Assert.IsTrue(_negAndPos.ValueWithMembership(1f, 0f, 2f).NearlyEquals(0f));
            Assert.IsTrue(_negAndPos.ValueWithMembership(.5f, 0f, 2f).NearlyEquals(1f));
            Assert.IsTrue(_negAndPos.ValueWithMembership(0f, 0f, 2f).NearlyEquals(2f));

            Assert.IsTrue(_posAndShort.ValueWithMembership(.5f, 2f, 3f).NearlyEquals(2f));
            Assert.IsTrue(_posAndShort.ValueWithMembership(.25f, 2f, 3f).NearlyEquals(2.5f));
            Assert.IsTrue(_posAndShort.ValueWithMembership(0f, 2f, 3f).NearlyEquals(3f));
        }
    }
}
