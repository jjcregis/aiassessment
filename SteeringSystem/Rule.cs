﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SteeringSystem
{
    /// <summary>
    /// An AND rule used in the fuzzy steering system.
    /// </summary>
    public class Rule
    {
        private MembershipFunction _distanceFunction;
        private MembershipFunction _deltaDistanceFunction;
        private MembershipFunction _steeringFunction;

        /// <summary>
        /// Gets the distance membership function.
        /// </summary>
        public MembershipFunction DistanceFunction
        {
            get { return _distanceFunction; }
        }

        /// <summary>
        /// Gets the delta distance membership function.
        /// </summary>
        public MembershipFunction DeltaDistanceFunction
        {
            get { return _deltaDistanceFunction; }
        }

        /// <summary>
        /// Gets the steering membership function.
        /// </summary>
        public MembershipFunction SteeringFunction
        {
            get { return _steeringFunction; }
        }

        /// <summary>
        /// Constructs a rule using two input membership functions and an output membership function.
        /// </summary>
        /// <param name="distanceFunction">The distance input membership function.</param>
        /// <param name="deltaDistanceFuntion">The delta distance input membership function.</param>
        /// <param name="steeringFunction">The steering output membership function.</param>
        public Rule(MembershipFunction distanceFunction, MembershipFunction deltaDistanceFuntion, MembershipFunction steeringFunction)
        {
            _distanceFunction = distanceFunction;
            _deltaDistanceFunction = deltaDistanceFuntion;
            _steeringFunction = steeringFunction;
        }
    }
}
