﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
namespace SteeringSystem
{
    /// <summary>
    /// A membership function with a triangular shape.
    /// </summary>
    public class TriangleMembershipFunction : MembershipFunction
    {
        private float _maximumMembership;
        private float _bottomLeft;
        private float _top;
        private float _bottomRight;

        private float _risingSlope;
        private float _risingYIntercept;
        private float _fallingSlope;
        private float _fallingYIntercept;

        /// <summary>
        /// Gets the area of the function.
        /// </summary>
        public override float Area
        {
            get { return .5f * (_bottomRight - _bottomLeft) * _maximumMembership; }
        }

        /// <summary>
        /// Gets the value of the centroid of the function.
        /// </summary>
        public override float Centroid
        {
            get { return (_bottomLeft + _top + _bottomRight) / 3; }
        }

        /// <summary>
        /// Gets a list of 2D points that make up the function. 
        /// </summary>
        public override List<PointF> Points
        {
            get
            {
                List<PointF> points = new List<PointF>();
                points.Add(new PointF((float)_bottomRight, 0));
                points.Add(new PointF((float)_top, (float)_maximumMembership));
                points.Add(new PointF((float)_bottomLeft, 0));

                return points;
            }
        }

        /// <summary>
        /// Constructs a new triangular membership function.
        /// </summary>
        /// <param name="bottomLeft">The value of the bottom left point.</param>
        /// <param name="top">The value of the top point.</param>
        /// <param name="bottomRight">The value of the bottom right point.</param>
        /// <param name="maximumMembership">The maximum membership of the function.</param>
        public TriangleMembershipFunction(float bottomLeft, float top, float bottomRight, float maximumMembership)
        {
            _maximumMembership = maximumMembership;
            _bottomLeft = bottomLeft;
            _top = top;
            _bottomRight = bottomRight;

            _risingSlope = _maximumMembership / (_top - _bottomLeft);
            _risingYIntercept = -_risingSlope * _bottomLeft;
            _fallingSlope = -_maximumMembership / (_bottomRight - _top);
            _fallingYIntercept = _maximumMembership - _fallingSlope * _top;
        }

        /// <summary>
        /// Returns the membership at a specified value on the function.
        /// </summary>
        /// <param name="x">The value.</param>
        /// <returns>The membership.</returns>
        public override float MembershipOf(float x)
        {
            float slope;
            float c;

            if (x < _bottomLeft || x > _bottomRight)
            {
                return 0f;
            }

            if (x <= _top)
            {
                slope = _risingSlope;
                c = _risingYIntercept;
            }
            else
            {
                slope = _fallingSlope;
                c = _fallingYIntercept;
            }

            return slope * x + c;
        }

        /// <summary>
        /// Determines the value with the specified membership value in the specified range.
        /// </summary>
        /// <param name="y">The membership.</param>
        /// <param name="minRange">The minimum range value.</param>
        /// <param name="maxRange">The maximum range value.</param>
        /// <returns>The value.</returns>
        public override float ValueWithMembership(float y, float minRange, float maxRange)
        {
            float x = (y - _risingYIntercept) / _risingSlope;
            if (x >= minRange && x <= maxRange)
            {
                return x;
            }

            x = (y - _fallingYIntercept) / _fallingSlope;
            return x;
        }

        /// <summary>
        /// Returns the shape formed using the specified membership.
        /// </summary>
        /// <param name="y">The membership.</param>
        /// <returns>The membership function at the specified membership.</returns>
        /// <remarks>Using a value of 1 would return this membership function.</remarks>
        public override MembershipFunction ShapeAt(float y)
        {
            float topLeft = ValueWithMembership(y, _bottomLeft, _top);
            float topRight = ValueWithMembership(y, _top, _bottomRight);

            if (y.NearlyEquals(_maximumMembership))
            {
                return this;
            }

            return new TrapezoidMembershipFunction(_bottomLeft, topLeft, topRight, _bottomRight, y);
        }
    }
}
