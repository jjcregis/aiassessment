﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace SteeringSystem
{
    /// <summary>
    /// A steering system controlled by a fuzzy inference system.
    /// </summary>
    public class FuzzySteeringSystem : SteeringSystemBase
    {
        //Distance membership functions
        private TrapezoidMembershipFunction _dLeft;
        private TriangleMembershipFunction _dZero;
        private TrapezoidMembershipFunction _dRight;

        //Delta distance membership functions
        private TrapezoidMembershipFunction _ddLeft;
        private TriangleMembershipFunction _ddZero;
        private TrapezoidMembershipFunction _ddRight;

        //Steering membership functions
        private TriangleMembershipFunction _sLeft;
        private TriangleMembershipFunction _sZero;
        private TriangleMembershipFunction _sRight;

        private List<Rule> _rules;

        /// <summary>
        /// Constructs a new fuzzy steering system.
        /// </summary>
        public FuzzySteeringSystem()
        {
            _rules = new List<Rule>();

            // Initialize membership functions;
            _dLeft = new TrapezoidMembershipFunction(-10, -9, -1, 0, 1);
            _dZero = new TriangleMembershipFunction(-1, 0, 1, 1);
            _dRight = new TrapezoidMembershipFunction(0, 1, 9, 10, 1);

            _ddLeft = new TrapezoidMembershipFunction(-3.5f, -3.49f, -.01f, 0, 1);
            _ddZero = new TriangleMembershipFunction(-.01f, 0, .01f, 1);
            _ddRight = new TrapezoidMembershipFunction(0, .01f, 3.49f, 3.5f, 1);

            _sLeft = new TriangleMembershipFunction(-.3f, -.2f, -.1f, 1);
            _sZero = new TriangleMembershipFunction(-.1f, 0, .1f, 1);
            _sRight = new TriangleMembershipFunction(.1f, .2f, .3f, 1);
            
            // Add rules
            _rules.Add(new Rule(_dLeft, _ddLeft, _sRight));
            _rules.Add(new Rule(_dLeft, _ddZero, _sRight));
            _rules.Add(new Rule(_dLeft, _ddRight, _sZero));

            _rules.Add(new Rule(_dZero, _ddLeft, _sRight));
            _rules.Add(new Rule(_dZero, _ddZero, _sZero));
            _rules.Add(new Rule(_dZero, _ddRight, _sLeft));

            _rules.Add(new Rule(_dRight, _ddLeft, _sZero));
            _rules.Add(new Rule(_dRight, _ddZero, _sLeft));
            _rules.Add(new Rule(_dRight, _ddRight, _sLeft));
            }

        /// <summary>
        /// Determines the steering value to apply to the vehicle given the inputs.
        /// </summary>
        /// <param name="distance">The distance from the racing line.</param>
        /// <param name="deltaDistance">The change in distance with respect to racing line.</param>
        /// <returns>The steering value to apply to the vehicle.</returns>
        public override float DetermineSteerValue(float distance, float deltaDistance)
        {
            List<Rule> usedRules = new List<Rule>();
            List<MembershipFunction> resultFunctions = new List<MembershipFunction>();

            // Determine the rules with output
            foreach (Rule rule in _rules)
            {
                if (rule.DistanceFunction.HasMembership(distance) || rule.DeltaDistanceFunction.HasMembership(deltaDistance))
                {
                    usedRules.Add(rule);
                }
            }

            // Maps steering membership functions to the result membership functions and only keeps the result functions with the highest area
            Dictionary<MembershipFunction, MembershipFunction> ruleResults = new Dictionary<MembershipFunction, MembershipFunction>();

            // For each rule with output, determine what the output function is.
            foreach (Rule rule in usedRules)
            {
                float resultValue = Math.Min(rule.DistanceFunction.MembershipOf(distance), rule.DeltaDistanceFunction.MembershipOf(deltaDistance));

                if (resultValue > 0f)
                {
                    MembershipFunction resultFunction = rule.SteeringFunction.ShapeAt(resultValue);
                    if (ruleResults.ContainsKey(rule.SteeringFunction))
                    {
                        // Keep result if the area is greater
                        if (resultFunction.Area > ruleResults[rule.SteeringFunction].Area)
                        {
                            ruleResults[rule.SteeringFunction] = resultFunction;
                        }
                    }
                    else
                    {
                        ruleResults.Add(rule.SteeringFunction, resultFunction);
                    }
                }
            }

            return CalculateCentroid(ruleResults.Values.ToList());
        }

        /// <summary>
        /// Calculates the centroid of the shape formed by the membership functions.
        /// </summary>
        /// <param name="functions">The membership funcitons.</param>
        /// <returns>The centroid of the shape.</returns>
        private float CalculateCentroid(List<MembershipFunction> functions)
        {
            float area = 0;
            float centroid = 0;
            List<PointF> points = new List<PointF>();

            foreach (MembershipFunction function in functions)
            {
                // Add the points from each function shape
                points.AddRange(function.Points);

                // Calculate the total area of the functions. This assumes that no functions overlap
                area += (float)function.Area;
            }

            // Calculate the centroid
            // http://www.seas.upenn.edu/~sys502/extra_materials/Polygon%20Area%20and%20Centroid.pdf
            for (int i = 0; i < points.Count - 1; i++)
            {
                centroid += ((points[i].X + points[i+1].X) * (points[i].X * points[i+1].Y - points[i+1].X * points[i].Y));
            }

            return centroid / (6 * area);
        }
    }
}
