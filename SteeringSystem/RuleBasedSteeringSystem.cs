﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SteeringSystem
{
    /// <summary>
    /// A steering system controlled by a rule based system.
    /// </summary>
    public class RuleBasedSteeringSystem : SteeringSystemBase
    {
        /// <summary>
        /// Constructs a new rule based steering system.
        /// </summary>
        public RuleBasedSteeringSystem()
        {
        }

        /// <summary>
        /// Determines the steering value to apply to the vehicle given the inputs.
        /// </summary>
        /// <param name="distance">The distance from the racing line.</param>
        /// <param name="deltaDistance">The change in distance with respect to racing line.</param>
        /// <returns>The steering value to apply to the vehicle.</returns>
        public override float DetermineSteerValue(float distance, float deltaDistance)
        {
            float steerValue = .1f;
            float stalledThreshold = .000075f;

            if (distance < 0)
            {
                if (Math.Abs(deltaDistance) < stalledThreshold)
                {
                    steerValue = .01f;
                }
                else
                {
                    steerValue *= -deltaDistance;
                }
            }
            else if (distance > 0)
            {
                if (Math.Abs(deltaDistance) < stalledThreshold)
                {
                    steerValue = -.01f;
                }
                else
                {
                    steerValue *= -deltaDistance;
                }
            }
            else
            {
                steerValue *= 0;
            }

            return steerValue;
        }
    }
}
