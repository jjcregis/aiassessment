﻿using System;

namespace SteeringSystem
{
    /// <summary>
    /// A collection of extension methods for floats.
    /// </summary>
    public static class FloatExtensions
    {
        /// <summary>
        /// Squares a float.
        /// </summary>
        /// <param name="number">The float.</param>
        /// <returns>The squared float.</returns>
        public static float Square(this float number)
        {
            return number * number;
        }

        /// <summary>
        /// Determines if a float is nearly equal to another float.
        /// </summary>
        /// <param name="n">The float.</param>
        /// <param name="number">The other float.</param>
        /// <returns>If the floats are nearly equal or not.</returns>
        public static bool NearlyEquals(this float n, float number)
        {
            return Math.Abs(n - number) < 0.00001f;
        }
    }
}