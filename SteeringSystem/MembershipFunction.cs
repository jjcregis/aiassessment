﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace SteeringSystem
{
    /// <summary>
    /// The base class for membership functions in a fuzzy inference system.
    /// </summary>
    public abstract class MembershipFunction
    {
        public abstract float Area { get; }
        public abstract float Centroid { get; }
        public abstract List<PointF> Points { get; }

        public abstract float MembershipOf(float x);
        public abstract float ValueWithMembership(float y, float minRange, float maxRange);
        public abstract MembershipFunction ShapeAt(float y);

        /// <summary>
        /// Returns if the value has membership in the function or not.
        /// </summary>
        /// <param name="x">The value.</param>
        /// <returns>If the value has membership or not.</returns>
        public bool HasMembership(float x)
        {
            return MembershipOf(x) > 0f;
        }

        
    }
}
