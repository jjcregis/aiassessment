﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SteeringSystem
{
    /// <summary>
    /// A base class for a steering system.
    /// </summary>
    public abstract class SteeringSystemBase
    {
        public abstract float DetermineSteerValue(float distance, float deltaDistance);
    }
}
