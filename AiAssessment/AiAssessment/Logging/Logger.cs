﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using AiAssessment;

namespace AiAssessment.Logging
{
    public class Logger
    {
        private readonly string _filePath = @"..\csv.txt";
        private FileStream _csvFile;
        private string _csvContent;
        private Car[] _cars;

        public Logger(Car[] cars)
        {
            _cars = cars;

            Initialise();
        }

        private void Initialise()
        {
            //_csvFile = File.Create(_filePath);
            _csvContent = "FuzzyDist,FuzzyDeltaDist,FuzzyOutput,RuleDist,RuleDeltaDist,RuleOutput\n";
        }

        public void Log()
        {
            for (int i = 0; i < _cars.Length; i++)
            {
                _csvContent += _cars[i].DistanceFromLine.ToString("F7") + ",";
                _csvContent += _cars[i].DeltaDistance.ToString("F7") + ",";
                _csvContent += _cars[i].DeltaRotation.ToString("F7");

                if (i < _cars.Length - 1) _csvContent += ",";
            }

            _csvContent += "\n";
        }

        public void Write()
        {
            File.WriteAllText(_filePath, _csvContent);
        }
    }
}
