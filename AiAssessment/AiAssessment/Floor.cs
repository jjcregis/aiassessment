﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AiAssessment.Graphics;

namespace AiAssessment
{
    public class Floor
    {
        private GraphicsComponent _graphics;

        public Floor(GraphicsComponent graphics)
        {
            _graphics = graphics;
            _graphics.BuildGeometry(this);
        }

        public void Update()
        {
        }

        public void Draw()
        {
            _graphics.Draw(this);
        }

    }
}
