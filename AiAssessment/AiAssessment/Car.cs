﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using AiAssessment.Graphics;
using SteeringSystem;

namespace AiAssessment
{
    public class Car
    {
        private GraphicsComponent _graphics;
        private RacingLine _racingLine;
        SteeringSystemBase _steeringSystem;
        private readonly float _speed = 1f/50f;

        private int _id;
        private float _rotation;
        private float _deltaRotation;
        private Vector3 _velocity;
        private Vector3 _position;

        private float _lastDistanceFromLine;
        private float _distanceFromLine;

        public int ID
        {
            get { return _id; }
        }

        public Vector3 Position
        {
            get { return _position; }
        }

        public float Rotation
        {
            get { return _rotation; }
        }

        public float DeltaRotation
        {
            get { return _deltaRotation; }
        }
        
        public Vector3 Velocity
        {
            get { return _velocity; }
        }

        public float DistanceFromLine
        {
            get { return _distanceFromLine; }
        }

        public float DeltaDistance
        {
            get { return _distanceFromLine - _lastDistanceFromLine; }
        }
        
        public Car(RacingLine racingLine, SteeringSystemBase steeringSystem, int id, GraphicsComponent graphics)
        {
            _graphics = graphics;
            _racingLine = racingLine;
            _steeringSystem = steeringSystem;
            _id = id;

            _position = new Vector3(0f, .5f, 0f);
            _deltaRotation = 0f;
            _rotation = 0f;
            _velocity = Vector3.Forward * _speed;            

            _graphics.BuildGeometry(this);
        }

        public void Update()
        {
            
            Quaternion deltaRotationQuaternion = Quaternion.CreateFromAxisAngle(Vector3.Down, _deltaRotation);
            _rotation += _deltaRotation;            

            // Change velocity direction
            _velocity = Vector3.Transform(_velocity, deltaRotationQuaternion);

            // Move car
            _position += _velocity;
            
            // Get the distance from the racing line for the next steering value
            _lastDistanceFromLine = _distanceFromLine;
            _distanceFromLine = _position.X - _racingLine.Position;

            if (_rotation > MathHelper.TwoPi)
            {
                _rotation -= MathHelper.TwoPi;
            }
            else if (_rotation < -MathHelper.TwoPi)
            {
                _rotation += MathHelper.TwoPi;
            }
            
            // Update geometry
            _graphics.Update(this);

            // Determine the steering value from the steering system
            _deltaRotation = _steeringSystem.DetermineSteerValue(_distanceFromLine, DeltaDistance);
        }

        public void Draw()
        {
            _graphics.Draw(this);
        }
    }
}
