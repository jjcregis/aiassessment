﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AiAssessment.Content
{
    public class ContentComponent
    {
        private ContentManager _contentManager;

        public ContentComponent(Game game)
        {
            Initialise(game);
        }

        private void Initialise(Game game)
        {
            _contentManager = game.Content;
            game.Content.RootDirectory = "Content";
        }

        public T Load<T>(string assetName)
        {
            T item = _contentManager.Load<T>(assetName);
            return item;
        }
    }
}
