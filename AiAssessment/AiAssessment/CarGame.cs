using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using AiAssessment.Graphics;
using AiAssessment.Input;
using AiAssessment.Content;
using AiAssessment.Logging;
using SteeringSystem;

namespace AiAssessment
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class CarGame : Microsoft.Xna.Framework.Game
    {
        readonly bool doLogging = false;

        GraphicsComponent graphics;
        ContentComponent content;
        InputComponent input;
        Logger logger;

        StatPanel statPanel;
        Camera camera;
        Car fuzzyCar;
        Car ruleCar;
        RacingLine racingLine;
        Floor floor;

        public CarGame()
        {
            graphics = new GraphicsComponent(this);
            content = new ContentComponent(this);
            input = new InputComponent();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            graphics.InitialiseGraphics(this);

            // Set Update rate to 100Hz
            TargetElapsedTime = TimeSpan.FromSeconds(1 / 100f);

            racingLine = new RacingLine(graphics, input);
            fuzzyCar = new Car(racingLine, new FuzzySteeringSystem(), 0, graphics);
            ruleCar = new Car(racingLine, new RuleBasedSteeringSystem(), 1, graphics);
            camera = new Camera(new Car[] {fuzzyCar, ruleCar}, graphics, input);
            floor = new Floor(graphics);
            statPanel = new StatPanel(new Car[] { fuzzyCar, ruleCar }, racingLine, graphics, input);
            logger = new Logger(new Car[] { fuzzyCar, ruleCar });

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Load a pixel texture to use
            graphics.Pixel = content.Load<Texture2D>(@"Textures\Pixel");
            graphics.SpriteFont = content.Load<SpriteFont>(@"Fonts\GuiFont");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            if(doLogging) logger.Write();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            input.Update(this);

            camera.Update();
            racingLine.Update();
            fuzzyCar.Update();
            ruleCar.Update();
            floor.Update();
            statPanel.Update();
            if(doLogging) logger.Log();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            graphics.Draw(this);
            graphics.BeginDraw();

            floor.Draw();
            racingLine.Draw();
            fuzzyCar.Draw();
            ruleCar.Draw();

            graphics.BeginSpriteDraw();
            statPanel.Draw();
            graphics.EndSpriteDraw();

            base.Draw(gameTime);
        }
    }
}
