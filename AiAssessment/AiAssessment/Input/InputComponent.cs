﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace AiAssessment.Input
{
    public class InputComponent
    {
        // Key mappings
        private readonly Keys _moveRightKey = Keys.Right;
        private readonly Keys _moveLeftKey = Keys.Left;
        private readonly Keys _increaseSpeedRightKey = Keys.OemPlus;
        private readonly Keys _increaseSpeedLeftKey = Keys.OemMinus;
        private readonly Keys _quitKey = Keys.Escape;
        private readonly Keys _selectFuzzyCarKey = Keys.D1;
        private readonly Keys _selectRuleCarKey = Keys.D2;

        // Device states
        private KeyboardState _lastKeyboardState;
        private KeyboardState _nowKeyboardState;
        private MouseState _lastMouseState;
        private MouseState _nowMouseState;

        public InputComponent()
        {
            Initialise();
        }

        private void Initialise()
        {
            _nowKeyboardState = Keyboard.GetState();
            _nowMouseState = Mouse.GetState();
        }

        public void Update(Game game)
        {
            // Update the device states
            _lastKeyboardState = _nowKeyboardState;
            _lastMouseState = _nowMouseState;
            _nowKeyboardState = Keyboard.GetState();
            _nowMouseState = Mouse.GetState();

            // Check if the quit button is pressed
            if (_nowKeyboardState.IsKeyDown(_quitKey))
            {
                game.Exit();
            }
        }

        public void Update(RacingLine racingLine)
        {
            if (!_nowKeyboardState.IsKeyDown(_moveLeftKey) && _lastKeyboardState.IsKeyDown(_moveLeftKey))
            {
                racingLine.Position--;
            }
            if (!_nowKeyboardState.IsKeyDown(_moveRightKey) && _lastKeyboardState.IsKeyDown(_moveRightKey))
            {
                racingLine.Position++;
            }
            if (!_nowKeyboardState.IsKeyDown(_increaseSpeedLeftKey) && _lastKeyboardState.IsKeyDown(_increaseSpeedLeftKey))
            {
                racingLine.MovementRate -= .1f;
            }
            if (!_nowKeyboardState.IsKeyDown(_increaseSpeedRightKey) && _lastKeyboardState.IsKeyDown(_increaseSpeedRightKey))
            {
                racingLine.MovementRate += .1f;
            }
        }

        public void Update(Camera camera)
        {
            if (_nowKeyboardState.IsKeyDown(_selectFuzzyCarKey))
            {
                camera.TargetCarIndex = 0;
            }
            else if (_nowKeyboardState.IsKeyDown(_selectRuleCarKey))
            {
                camera.TargetCarIndex = 1;
            }
        }

        public void Update(StatPanel statPanel)
        {
            if (_nowKeyboardState.IsKeyDown(_selectFuzzyCarKey))
            {
                statPanel.TargetCarIndex = 0;
            }
            else if (_nowKeyboardState.IsKeyDown(_selectRuleCarKey))
            {
                statPanel.TargetCarIndex = 1;
            }
        }
    }
}
