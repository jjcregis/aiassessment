﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using AiAssessment.Graphics;
using AiAssessment.Input;

namespace AiAssessment
{
    public class Camera
    {
        private GraphicsComponent _graphics;
        private InputComponent _input;

        private Car[] _targetCars;
        private int _targetCarIndex;
        private Vector3 _offsetPosition = new Vector3(1f, 4f, 5f);

        private Matrix _worldMatrix;
        private Matrix _viewMatrix;
        private Matrix _projectionMatrix;

        private float _fieldOfView = (float)Math.PI / 3;
        private float _aspectRatio = 1.66f;
        private float _nearClippingDistance = 0.01f;
        private float _farClippingDistance = 100f;

        public Car[] TargetCars
        {
            get { return _targetCars; }
        }

        public int TargetCarIndex
        {
            get { return _targetCarIndex; }
            set { _targetCarIndex = value; }
        }

        public Matrix WorldMatrix
        {
            get { return _worldMatrix; }
            set { _worldMatrix = value; }
        }

        public Matrix ViewMatrix
        {
            get { return _viewMatrix; }
            set { _viewMatrix = value; }
        }

        public Matrix ProjectionMatrix
        {
            get { return _projectionMatrix; }
            set { _projectionMatrix = value; }
        }

        public Camera(Car[] targets, GraphicsComponent graphics, InputComponent input)
        {
            _graphics = graphics;
            _targetCars = targets;
            _input = input;

            Initialise();
        }

        private void Initialise()
        {
            _targetCarIndex = 0;
            _worldMatrix = Matrix.Identity;
            _projectionMatrix = Matrix.CreatePerspectiveFieldOfView(_fieldOfView, _aspectRatio, _nearClippingDistance, _farClippingDistance);
            _viewMatrix = Matrix.CreateLookAt(_targetCars[_targetCarIndex].Position + _offsetPosition, _targetCars[_targetCarIndex].Position, Vector3.Up);
        }
        public void Update()
        {
            _graphics.Update(this);
            _input.Update(this);
            _viewMatrix = Matrix.CreateLookAt(_targetCars[_targetCarIndex].Position + _offsetPosition, _targetCars[_targetCarIndex].Position, Vector3.Up);
        }
    }
}
