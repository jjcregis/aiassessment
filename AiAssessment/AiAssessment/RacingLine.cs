﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AiAssessment.Graphics;
using AiAssessment.Input;

namespace AiAssessment
{
    public class RacingLine
    {
        private GraphicsComponent _graphics;
        private InputComponent _input;

        private float _position;
        private float _movementRate;

        public float Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public float MovementRate
        {
            get { return _movementRate; }
            set { _movementRate = value; }
        }
            
        public RacingLine(GraphicsComponent graphics, InputComponent input)
        {
            _graphics = graphics;
            _input = input;

            _position = 0f;
            _movementRate = 0;

            _graphics.BuildGeometry(this);
        }

        public void Update()
        {
            _position += _movementRate * 1/100f;
            _input.Update(this);
            _graphics.Update(this);
        }

        public void Draw()
        {
            _graphics.Draw(this);
        }
    }
}
