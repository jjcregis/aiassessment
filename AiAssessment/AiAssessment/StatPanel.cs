﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AiAssessment;
using AiAssessment.Graphics;
using AiAssessment.Input;
using Microsoft.Xna.Framework;

namespace AiAssessment
{
    public class StatPanel
    {
        private GraphicsComponent _graphics;
        private InputComponent _input;

        private Car[] _cars;
        private int _carIndex;
        private RacingLine _racingLine;

        public Car[] Cars
        {
            get { return _cars; }
        }

        public int TargetCarIndex
        {
            get { return _carIndex; }
            set { _carIndex = value; }
        }

        public Car TargetCar
        {
            get { return _cars[_carIndex]; }
        }

        public RacingLine RacingLine
        {
            get { return _racingLine; }
        }

        public StatPanel(Car[] cars, RacingLine racingLine, GraphicsComponent graphics, InputComponent input)
        {
            _cars = cars;
            _racingLine = racingLine;
            _graphics = graphics;
            _input = input;
        }

        public void Update()
        {
            _input.Update(this);
        }

        public void Draw()
        {
            _graphics.Draw(this);
        }
    }
}
