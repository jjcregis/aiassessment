﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AiAssessment.Graphics
{
    public class GraphicsComponent
    {
        private BasicEffect _effect;
        private SpriteBatch _spriteBatch;
        private GraphicsDeviceManager _deviceManager;
        private EffectPass _pass;

        private float _viewportHeight;
        private float _viewportWidth;

        private Texture2D _pixel;
        private SpriteFont _spriteFont;
        
        // Car Vertices
        private VertexPositionColor[] _fuzzyCarPoints;
        private VertexPositionColor[] _ruleCarPoints;
        private int[] _carIndices;

        // Floor Vertices
        private VertexPositionColor[] _floorPoints;
        private int[] _floorIndices;

        //Racing Line Vertices
        private VertexPositionColor[] _linePoints;

        public SpriteFont SpriteFont
        {
            get { return _spriteFont; }
            set { _spriteFont = value; }
        }
        
        public Texture2D Pixel
        {
            get { return _pixel; }
            set { _pixel = value; }
        }

        public GraphicsComponent(Game game)
        {
            Initialise(game);
        }

        void Initialise(Game game)
        {
            _deviceManager = new GraphicsDeviceManager(game);
            _deviceManager.PreferMultiSampling = true;
        }

        public void InitialiseGraphics(Game game)
        {
            _spriteBatch = new SpriteBatch(game.GraphicsDevice);
            _effect = new BasicEffect(game.GraphicsDevice);
            _viewportHeight = _deviceManager.GraphicsDevice.Viewport.Height;
            _viewportWidth = _deviceManager.GraphicsDevice.Viewport.Width;
        }

        public void BuildGeometry(Floor floor)
        {
            List<VertexPositionColor> pointList = new List<VertexPositionColor>();
            List<int> indexList = new List<int>();

            int resolution = 1000;
            Color floorColor = new Color(64, 64, 64);

            for (int i = 0; i < resolution; i++)
            {
                // Z-direction lines
                pointList.Add(new VertexPositionColor(new Vector3(-i, 0, -resolution), floorColor));
                pointList.Add(new VertexPositionColor(new Vector3(-i, 0, resolution), floorColor));
                pointList.Add(new VertexPositionColor(new Vector3(i, 0, -resolution), floorColor));
                pointList.Add(new VertexPositionColor(new Vector3(i, 0, resolution), floorColor));

                //X-direction lines
                pointList.Add(new VertexPositionColor(new Vector3(-resolution, 0, -i), floorColor));
                pointList.Add(new VertexPositionColor(new Vector3(resolution, 0, -i), floorColor));
                pointList.Add(new VertexPositionColor(new Vector3(-resolution, 0, i), floorColor));
                pointList.Add(new VertexPositionColor(new Vector3(resolution, 0, i), floorColor));

                indexList.AddRange(new int[]
                {   0 + i * 8, 1 + i * 8, 2 + i * 8, 3 + i * 8,
                    4 + i * 8, 5 + i * 8, 6 + i * 8, 7 + i * 8
                });
            }

            _floorPoints = pointList.ToArray();
            _floorIndices = indexList.ToArray();
        }

        public void BuildGeometry(Car car)
        {
            float size = 1;
            Vector3 vertexOffset = new Vector3(-.5f, -.5f, -.5f);
            Color carColour;
            vertexOffset += car.Position;

            if (car.ID == 0)
            {
                carColour = Color.CornflowerBlue;
                _fuzzyCarPoints = new VertexPositionColor[]
                {
                    new VertexPositionColor(new Vector3(0, size, size) + vertexOffset, carColour),
                    new VertexPositionColor(new Vector3(size, size, size) + vertexOffset, carColour),
                    new VertexPositionColor(new Vector3(size, 0, size) + vertexOffset, carColour),
                    new VertexPositionColor(new Vector3(0, 0, size) + vertexOffset, carColour),

                    new VertexPositionColor(new Vector3(0, size, 0) + vertexOffset, carColour),
                    new VertexPositionColor(new Vector3(size, size, 0) + vertexOffset, carColour),
                    new VertexPositionColor(new Vector3(size, 0, 0) + vertexOffset, carColour),
                    new VertexPositionColor(new Vector3(0, 0, 0) + vertexOffset, carColour),
                
                };
            }
            else if (car.ID == 1)
            {
                carColour = Color.Orange;

                _ruleCarPoints = new VertexPositionColor[]
                {
                    new VertexPositionColor(new Vector3(0, size, size) + vertexOffset, carColour),
                    new VertexPositionColor(new Vector3(size, size, size) + vertexOffset, carColour),
                    new VertexPositionColor(new Vector3(size, 0, size) + vertexOffset, carColour),
                    new VertexPositionColor(new Vector3(0, 0, size) + vertexOffset, carColour),

                    new VertexPositionColor(new Vector3(0, size, 0) + vertexOffset, carColour),
                    new VertexPositionColor(new Vector3(size, size, 0) + vertexOffset, carColour),
                    new VertexPositionColor(new Vector3(size, 0, 0) + vertexOffset, carColour),
                    new VertexPositionColor(new Vector3(0, 0, 0) + vertexOffset, carColour),
                
                };
            }

            _carIndices = new int[] {
                0,1,1,2,2,3,3,0,
                4,5,5,6,6,7,7,4,
                0,4,1,5,2,6,3,7
            };
        }
        
        public void BuildGeometry(RacingLine racingLine)
        {
            Color lineColor = Color.Red;

            _linePoints = new VertexPositionColor[]
            {
                new VertexPositionColor(new Vector3(0f, .025f, 1000f), lineColor),
                new VertexPositionColor(new Vector3(0f, .025f, -1000f), lineColor)
            };
        }

        public void Update(Camera camera)
        {
            _effect.World = camera.WorldMatrix;
            _effect.Projection = camera.ProjectionMatrix;
            _effect.View = camera.ViewMatrix;
        }

        public void Draw(Game game)
        {
            _deviceManager.GraphicsDevice.Clear(Color.Black);
        }

        public void Draw(Floor floor)
        {
            // Effect setup
            _effect.VertexColorEnabled = true;
            _pass.Apply();

            _deviceManager.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(PrimitiveType.LineList, _floorPoints, 0, _floorPoints.Length, _floorIndices, 0, _floorIndices.Length / 2);

            // Effect reset
            _effect.VertexColorEnabled = false;
        }

        public void Draw(StatPanel statPanel)
        {
            Vector2 padding = new Vector2(10, 10);

            // Draw panel background
            _spriteBatch.Draw(_pixel, new Rectangle(0, 0, (int)(_viewportWidth / 4), (int)_viewportHeight), new Color(0, 0, 0, 128));

            _spriteBatch.DrawString(_spriteFont, "Line Position\n" + statPanel.RacingLine.Position.ToString("F3") + " units", padding, Color.White);
            padding.Y += 60;
            _spriteBatch.DrawString(_spriteFont, "Line Velocity\n" + statPanel.RacingLine.MovementRate.ToString("F1") + " units/s", padding, Color.White);
            padding.Y += 90;
            _spriteBatch.DrawString(_spriteFont, "Distance From Line\n" + statPanel.TargetCar.DistanceFromLine.ToString("F7") + " units", padding, Color.White);
            padding.Y += 60;
            _spriteBatch.DrawString(_spriteFont, "Distance Delta\n" + statPanel.TargetCar.DeltaDistance.ToString("F7") + " units", padding, Color.White);
            padding.Y += 60;
            _spriteBatch.DrawString(_spriteFont, "Rotation Delta\n" + statPanel.TargetCar.DeltaRotation.ToString("F7") + " radians", padding, Color.White);

        }

        public void BeginDraw()
        {
            _pass = _effect.CurrentTechnique.Passes[0];
        }

        public void BeginSpriteDraw()
        {
            _spriteBatch.Begin();
        }
        
        public void EndSpriteDraw()
        {
            _spriteBatch.End();
        }

        public void Draw(Car car)
        {
            // Get the vertices for the car being drawn
            VertexPositionColor[] carPoints = null;
            if (car.ID == 0) carPoints = _fuzzyCarPoints;
            else if (car.ID == 1) carPoints = _ruleCarPoints;

            // Effect setup
            _effect.VertexColorEnabled = true;
            _pass.Apply();

            _deviceManager.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(PrimitiveType.LineList, carPoints, 0, carPoints.Length, _carIndices, 0, _carIndices.Length / 2);

            // Effect reset
            _effect.VertexColorEnabled = false;
        }

        public void Draw(RacingLine racingLine)
        {
            // Effect setup
            _effect.VertexColorEnabled = true;
            _pass.Apply();

            _deviceManager.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, _linePoints, 0, 1);

            // Effect reset
            _effect.VertexColorEnabled = false;
        }

        public void Update(Car car)
        {
            // Get the vertices for the car being drawn
            VertexPositionColor[] carPoints = null;
            if (car.ID == 0) carPoints = _fuzzyCarPoints;
            else if (car.ID == 1) carPoints = _ruleCarPoints;

            Quaternion rotation = Quaternion.CreateFromAxisAngle(Vector3.Down, car.DeltaRotation);

            for (int i = 0; i < carPoints.Length; i++)
            {
                // Advance point
                carPoints[i].Position += car.Velocity;
                
                // Translate to origin
                carPoints[i].Position -= car.Position;

                // Rotate
                carPoints[i].Position = Vector3.Transform(carPoints[i].Position, rotation);

                // Translate back
                carPoints[i].Position += car.Position;              
            }
        }

        public void Update(RacingLine racingLine)
        {
            for (int i = 0; i < _linePoints.Length; i++)
            {
                _linePoints[i].Position.X = racingLine.Position;
            }
        }
    }
}
