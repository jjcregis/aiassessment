﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SteeringSystem;
namespace SteeringSystemTestApplication
{
    /// <summary>
    /// A test application for testing values returned by the fuzzy inference system.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            FuzzySteeringSystem fuzzySystem = new FuzzySteeringSystem();
            float distance;
            float deltaDistance;
            float steerValue;
            string line;

            while (true)
            {
                Console.WriteLine("Enter car distance from line:");
                line = Console.ReadLine();
                if (!float.TryParse(line, out distance))
                {
                    Console.WriteLine("Parsing error.");
                }
                else
                {
                    Console.WriteLine("Enter change in distance from line:");
                    line = Console.ReadLine();
                    if (!float.TryParse(line, out deltaDistance))
                    {
                        Console.WriteLine("Parsing error.");
                    }
                    else
                    {
                        // Compute the output from the system
                        steerValue = fuzzySystem.DetermineSteerValue(distance, deltaDistance);
                        Console.WriteLine("Computed steering value:\n" + steerValue.ToString("F7"));
                    }
                }

                Console.WriteLine("\n\n\n");
            }
        }
    }
}
